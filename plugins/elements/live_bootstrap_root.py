# Copyright (c) 2025 Dor Askayo
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
# Authors:
#       Dor Askayo <dor.askayo@gmail.com>

import os

from buildstream.element import Element

class LiveBootstrapRoot(Element):

    BST_MIN_VERSION = "2.0"

    PLUGIN_VERSION = 0

    BUILD_ROOT = "buildstream-build"
    INSTALL_ROOT = "buildstream-install"

    def configure(self, node):
        node.validate_keys([])

    def get_unique_key(self):
        return { "plugin_version": LiveBootstrapRoot.PLUGIN_VERSION }

    def preflight(self):
        pass

    def stage(self, sandbox):
        self.stage_dependency_artifacts(sandbox)

        if any(self.sources()):
            self.stage_sources(sandbox, LiveBootstrapRoot.BUILD_ROOT)

    def configure_sandbox(self, sandbox):
        pass

    def assemble(self, sandbox):
        install_root = LiveBootstrapRoot.INSTALL_ROOT

        with self.timed_activity("Creating bootstrap root directory"):
            build_root = LiveBootstrapRoot.BUILD_ROOT

            sandbox_vroot = sandbox.get_virtual_directory()

            # See "Without using Python:" in https://github.com/fosslinux/live-bootstrap/blob/master/README.rst
            import_directory_contents_map = {
                "seed/stage0-posix": ""
            }
            for key, value in import_directory_contents_map.items():
                source_dir = os.path.join(build_root, key)
                source_vdirectory = sandbox_vroot.open_directory(source_dir.lstrip(os.sep), create=False)
                destination_dir = os.path.join(install_root, value)
                destination_vdirectory = sandbox_vroot.open_directory(destination_dir.lstrip(os.sep), create=True)

                destination_vdirectory.import_files(source_vdirectory, collect_result=False)

            import_directory_files_map = {
                "seed": ""
            }
            for key, value in import_directory_files_map.items():
                include_files = []
                source_dir = os.path.join(build_root, key)
                source_vdirectory = sandbox_vroot.open_directory(source_dir.lstrip(os.sep), create=False)
                destination_dir = os.path.join(install_root, value)
                destination_vdirectory = sandbox_vroot.open_directory(destination_dir.lstrip(os.sep), create=True)

                def is_file(path):
                    return os.path.dirname(path) == "" and source_vdirectory.isfile(path)

                destination_vdirectory.import_files(source_vdirectory, filter_callback=is_file, collect_result=False)

            import_directories_map = {
                "steps": "steps",
                "distfiles": "external/distfiles"
            }
            for key, value in import_directories_map.items():
                source_dir = os.path.join(build_root, key)
                source_vdirectory = sandbox_vroot.open_directory(source_dir.lstrip(os.sep), create=False)
                destination_dir = os.path.join(install_root, value)
                destination_vdirectory = sandbox_vroot.open_directory(destination_dir.lstrip(os.sep), create=True)

                destination_vdirectory.import_files(source_vdirectory, collect_result=False)

            return install_root

# Plugin entry point
def setup():
    return LiveBootstrapRoot
