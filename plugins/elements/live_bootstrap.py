# Copyright (c) 2023,2025 Dor Askayo
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
# Authors:
#       Dor Askayo <dor.askayo@gmail.com>

import os

from buildstream.element import ElementError
from .command import AbstractCommand

class LiveBootstrap(AbstractCommand):

    BST_MIN_VERSION = "2.0"

    PLUGIN_VERSION = 0

    CONFIG_DIRECTORY="steps"

    def extra_configure(self, node):
        self._arch = node.get_str("arch")
        self._jobs = node.get_str("jobs")

    def get_extra_config_keys(self):
        return ["system", "arch", "jobs"]

    def get_extra_unique_key(self):
        key = {
            "plugin_version": LiveBootstrap.PLUGIN_VERSION,
            "arch": self._arch
        }

        return key

    def extra_stage(self, sandbox):
        build_root = self.get_variable("build-root")

        directory = os.path.join(build_root, LiveBootstrap.CONFIG_DIRECTORY)

        sandbox_vroot = sandbox.get_virtual_directory()
        host_vdirectory = sandbox_vroot.open_directory(directory.lstrip(os.sep), create=True)

        with host_vdirectory.open_file("bootstrap.cfg", mode="w") as config_file:
            config_file.writelines([
                f"ARCH={self._arch}\n",
                f"ARCH_DIR={self._arch}\n",
                "FORCE_TIMESTAMPS=False\n",
                "CHROOT=True\n",
                "UPDATE_CHECKSUMS=False\n",
                f"JOBS={self._jobs}\n",
                "SWAP_SIZE=0\n",
                f"FINAL_JOBS={self._jobs}\n",
                "INTERNAL_CI=False\n",
                "INTERACTIVE=False\n",
                "BARE_METAL=False\n",
                "DISK=sda1\n",
                "KERNEL_BOOTSTRAP=False\n",
                "BUILD_KERNELS=False\n",
                "CONFIGURATOR=False"
            ])

def setup():
    return LiveBootstrap
