# Copyright (c) 2023 Seppo Yli-Olli
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
# Authors:
#       Seppo Yli-Olli <seppo.yliolli@gmail.com>

import shlex

from buildstream.element import Element
from buildstream.sandbox import SandboxCommandError

class AbstractCommand(Element):

    def configure(self, node):
        config_keys = ["command", "root-read-only"]
        config_keys.extend(self.get_extra_config_keys())
        node.validate_keys(config_keys)

        self._command = node.get_str("command")
        self._root_read_only = node.get_bool("root-read-only", default=False)

        self.extra_configure(node)

    def extra_configure(self, node):
        pass

    def get_extra_config_keys(self):
        return []

    def get_unique_key(self):
        key = {
            "command": self._command,
            "root_read_only": self._root_read_only
        }

        build_root = self.get_variable("build-root")
        if build_root:
            key["build-root"] = build_root

        install_root = self.get_variable("install-root")
        if install_root:
            key["install-root"] = install_root

        key.update(self.get_extra_unique_key())

        return key

    def get_extra_unique_key(self):
        return {}

    def preflight(self):
        pass

    def stage(self, sandbox):
        self.stage_dependency_artifacts(sandbox)
        
        if any(self.sources()):
            self.stage_sources(sandbox, self.get_variable("build-root"))

        self.extra_stage(sandbox)

    def extra_stage(self, sandbox):
        pass

    def configure_sandbox(self, sandbox):
        build_root = self.get_variable("build-root")
        install_root = self.get_variable("install-root")

        sandbox.mark_directory(build_root)
        sandbox.mark_directory(install_root)

        sandbox.set_work_directory(build_root)

        sandbox.set_environment({"PATH": "/usr/bin"})

    def assemble(self, sandbox):
        with self.timed_activity(f"Running command", detail=f"{self._command}"):
          ret = sandbox.run(shlex.split(self._command), root_read_only=self._root_read_only)
          if ret != 0:
              raise SandboxCommandError(f"Failed command {self._command}")

        self.extra_assemble(sandbox)

        return self.get_variable("install-root")

    def extra_assemble(self, sandbox):
        pass

class Command(AbstractCommand):

    BST_MIN_VERSION = "2.0"

    PLUGIN_VERSION = 0

    def get_extra_unique_key(self):
        return { "plugin_version": Command.PLUGIN_VERSION }

# Plugin entry point
def setup():
    return Command
