#!/usr/bin/bash

set -e

REPO=/external/repo

unpack() {
  package="$1"
  stage="$2"

  if [ -n "$stage" ]; then
    archive="${REPO}/${package}_${stage}.tar.bz2"
  else
    archive="$(ls ${REPO}/${package}_*.tar.bz2 | sort | tail -1)"
  fi

  echo "Unpacking ${archive}"
  tar --keep-directory-symlink -xf "${archive}" -C /output
}

ls -l "${REPO}"

mkdir -p /output/usr/bin
ln -s usr/bin /output/bin
ln -s usr/bin /output/sbin
ln -s bin /output/usr/sbin

mkdir -p /output/usr/lib
ln -s usr/lib /output/lib

mkdir -p /output/var
mkdir -p /output/run
ln -s ../run /output/var/run

unpack bzip2-1.0.8
unpack autoconf-2.69
unpack flex-2.6.4
unpack help2man-1.36.4
unpack findutils-4.2.33
unpack linux-headers-4.14.341-openela
unpack util-linux-2.19.1
unpack dhcpcd-10.0.1
unpack bash-5.2.15
unpack xz-5.4.1
unpack file-5.44
unpack libtool-2.4.7
unpack tar-1.34
unpack coreutils-9.4
unpack pkg-config-0.29.2
unpack make-4.2.1
unpack gmp-6.2.1
unpack autoconf-archive-2021.02.19
unpack mpfr-4.1.0
unpack mpc-1.2.1
unpack bison-3.4.2
unpack dist-3.5-236
unpack perl-5.32.1
unpack libarchive-3.5.2
unpack openssl-3.0.13
unpack ca-certificates-3.99
unpack curl-8.5.0
unpack zlib-1.2.13
unpack automake-1.16.3
unpack autoconf-2.71
unpack patch-2.7.6
unpack gettext-0.21
unpack texinfo-6.7
unpack binutils-2.41
unpack gperf-3.1
unpack libunistring-0.9.10
unpack libffi-3.3
unpack libatomic_ops-7.6.10
unpack gc-8.0.4
unpack guile-3.0.9
unpack which-2.21
unpack grep-3.7
unpack sed-4.8
unpack autogen-5.18.16
unpack musl-1.2.4
unpack python-3.11.1
unpack gcc-13.1.0
unpack gzip-1.13
unpack diffutils-3.10
unpack gawk-5.3.0
unpack m4-1.4.19
